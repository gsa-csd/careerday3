# Career Fair 2023

## Intro

Welcome to Career Fair 2023 event's website! 

This app was implemented using Angular 16 and Bootstrap 5 (upgrade versions at your own risk).


## Content Update

Our main goal was to decouple the UI from the content. Therefore, UI changes do have to be made from the app's code, while content changes have to be performed directly to the respective json files in the `career-fair-2023/src/assets/` folder. You may need some time to navigate through it, but it shouldn't be too hard.

After any changes have been made, if you need to update the online website, make sure to have these changes on the main branch, and after pushing the update, a build pipeline will start running. This pipeline is configured so that GitLab pages builds the app and deploys it on https://gsa-csd.gitlab.io/careerday3/. 

If a domain has been bought and the DNS is set up to point to this URL, you should be able to access the updated page through this domain as well.

## Scripts 

In order to streamline companies details parsing, a python script has been implemented in folder `scripts/` that given the correct csv file downloaded from the online google sheets spreadsheet, generates the JSON with the companies data and downloads the respective images. 

---
### Setup environment (suggested)

Create a virtual environment
```bash
cd scripts
python3 -m venv .

# if you are working on a UNIX machine:
source bin/activate

# if you are working on a Windows machine:
source Script/activate
```

Install all dependencies
```bash
pip3 install -r requirements.txt
```

To deactivate virtual environment
```bash
deactivate
```
---
### Extract company information from registration *(google sheet)*

1. Download [registration sheet](https://docs.google.com/spreadsheets/d/1BkVlw5DJmC513WQ2CKPH-oguui42a8Op7B-CYWrXJY4/edit#gid=1384941389) as .csv (change the URL to your new spreasheet, that will have the same format as 2023's)
2. Put it in '/scripts/data'
3. Name it 'company_registration_info.csv'
4. From '/scripts' run `python3 format_registration_info.py`

First, google will ask for permissions in your google drive (Accept)

Then the program will extract all information from the csv and it will create:

1. companies: base folder
2. metadata.json: with all the information for the companies 
3. One folder with the name of each company, with:
<br>3.1 logo.webp
<br>3.2 name_surname.webp, for each member 


FYI: media files are stored in .webp format in order to decrease loading times as much as possible. As media files increase in number, loading times would get worser and worser :) 

After creating the aforementioned files, you may copy the companies folder in `career-fair-2023/src/assets/` folder and the new data will be loaded to the website.

**PRO TIP**! Αs the companies get more and more, it is strongly suggested that you only download the new ones so that the parsing will take significantly shorter. After that, you may copy the metadata.json file's content in the original `career-fair-2023/src/assets/companies/metadata.json` and only copy the new companies folders in the `career-fair-2023/src/assets/companies/` folder.
