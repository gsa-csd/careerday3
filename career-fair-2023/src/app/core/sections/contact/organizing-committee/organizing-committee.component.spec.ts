import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizingCommitteeComponent } from './organizing-committee.component';

describe('OrganizingCommitteeComponent', () => {
  let component: OrganizingCommitteeComponent;
  let fixture: ComponentFixture<OrganizingCommitteeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizingCommitteeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrganizingCommitteeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
