import { Component } from '@angular/core';
import { OrganizerModel } from 'src/app/global/models/organizer/organizer.model';
import { OrganizersService } from 'src/app/global/services/organizers/organizers.service';
import { VolunteersService } from 'src/app/global/services/volunteers/volunteers.service';

@Component({
  selector: 'app-organizing-committee',
  templateUrl: './organizing-committee.component.html',
  styleUrls: ['./organizing-committee.component.scss']
})
export class OrganizingCommitteeComponent {
  public organizers: OrganizerModel[] = [];
  public volunteers: OrganizerModel[] = [];
  constructor(private organizersService: OrganizersService, private volunteersService: VolunteersService) { }

  ngOnInit(): void {
    this.organizers = this.organizersService.getOrganizers();
    this.volunteers = this.volunteersService.getVolunteers();
  }
}
