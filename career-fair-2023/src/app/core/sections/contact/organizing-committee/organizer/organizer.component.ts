import { Component, Input } from '@angular/core';
import { OrganizerModel } from 'src/app/global/models/organizer/organizer.model';

@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.component.html',
  styleUrls: ['./organizer.component.scss']
})
export class OrganizerComponent {
  @Input() organizer!: OrganizerModel;
  constructor() { }

  ngOnInit(): void {
  }
}
