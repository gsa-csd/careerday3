import { Component, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-faq-card',
  templateUrl: './faq-card.component.html',
  styleUrls: ['./faq-card.component.scss'],
  animations: [
    trigger('fadeIn', [
      state('void', style({ opacity: 0 })),
      transition(':enter', [
        animate('0.3s ease-in-out', style({ opacity: 1 }))
      ]),
    ])
  ]
})
export class FaqCardComponent {
  @Input() faq: any;
  public isCollapsed = true;
  constructor() { }

  public toggleFAQ() {
    this.isCollapsed = !this.isCollapsed;
  }

}
