import { Component, OnInit } from '@angular/core';
import { FaqModel } from 'src/app/global/models/faq/faq.model';
import { FaqsService } from 'src/app/global/services/faqs/faqs.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  public faqs: FaqModel[] = []

  constructor(private faqsService: FaqsService) {

  }

  ngOnInit(): void {
    this.faqs = this.faqsService.getFaqs();
  }



}
