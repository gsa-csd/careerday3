import { Component, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CompanyModel } from 'src/app/global/models/company/company.model';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent {
  public company!: CompanyModel;

  constructor(
    public dialogRef: MatDialogRef<CompanyDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CompanyModel) {
      this.company = data;
    }

    show(){
    }

    onClose(): void {
      this.dialogRef.close();
    }

}
