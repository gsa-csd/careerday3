import { Component, Input } from '@angular/core';
import { StaffModel } from 'src/app/global/models/company/staff.model';

@Component({
  selector: 'app-staff-bubble',
  templateUrl: './staff-bubble.component.html',
  styleUrls: ['./staff-bubble.component.scss']
})
export class StaffBubbleComponent {
  @Input() staff!: StaffModel;
  @Input() marginTop!: number;
  constructor() { }

  ngOnInit(): void {
    this.marginTop = -10;
  }
}
