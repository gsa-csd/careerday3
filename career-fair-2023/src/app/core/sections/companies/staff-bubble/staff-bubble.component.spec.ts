import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffBubbleComponent } from './staff-bubble.component';

describe('StaffBubbleComponent', () => {
  let component: StaffBubbleComponent;
  let fixture: ComponentFixture<StaffBubbleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StaffBubbleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StaffBubbleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
