import { Component, Input } from '@angular/core';
import { StaffModel } from 'src/app/global/models/company/staff.model';

@Component({
  selector: 'app-staff-card',
  templateUrl: './staff-card.component.html',
  styleUrls: ['./staff-card.component.scss']
})
export class StaffCardComponent {
    @Input() staff!: StaffModel;
    constructor() { }

    ngOnInit(): void {
        this.staff.socialMedia = this.staff.socialMedia.filter(s => s.name != "Email"); // remove to add emails of staff
    }
}
