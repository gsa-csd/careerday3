import { Component } from '@angular/core';
import { CompanyModel } from 'src/app/global/models/company/company.model';
import { StaffModel } from 'src/app/global/models/company/staff.model';
import { CompaniesService } from 'src/app/global/services/companies/companies.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent {
  public companies: CompanyModel[] = [];
  public companiesPublic: boolean = true; // change to true to view
  public showPlaceholder: boolean = false;

  private state: "placeholder" | "testing" | "all" | "none" = "all";
  constructor(
    private companiesService: CompaniesService,
  ) { }

  ngOnInit(): void {
    this.companies = this.companiesService.getCompanies();
    switch (this.state) {
      case "placeholder":
        this.companies = this.companies.filter(company => company.name === "Placeholder Company");
        break;
      case "testing":
        this.companies = this.companies.filter(company => company.name !== "Testing Company");
        break;
      case "all":
        this.companies = this.companies.filter(company => company.name !== "Public Company");
        break;
      case "none":
        this.companiesPublic = false;
        break;
      }
  }
}

