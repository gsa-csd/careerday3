import { Component, Inject, Input } from '@angular/core';
import { CompanyModel } from 'src/app/global/models/company/company.model';
import { MatDialog } from '@angular/material/dialog';
import { CompanyDetailsComponent } from '../company-details/company-details.component';
import { ParamsHandlerService } from 'src/app/global/services/params-handler/params-handler.service';
@Component({
  selector: 'app-company-card',
  templateUrl: './company-card.component.html',
  styleUrls: ['./company-card.component.scss']
})
export class CompanyCardComponent {
  @Input() company!: CompanyModel;
  @Input() width!: number;
  @Input() height!: number;
  @Input() showStaff: boolean = false;
  @Input() cardSize: "small" | "medium" | "large" = "medium";
  @Input() isSponsor: boolean = false;
  public minWidth!: string;
  public minHeight!: string;
  public logoHeight!: number;
  // all size have aspect ratio of 3/2
  public sizes: {width: number, height: number, minWidth: string, minHeight: string}[] = [
    {width: 192, height: 128, minWidth: '70vw', minHeight: '15vh' },
    {width: 224, height: 150, minWidth: '80vw', minHeight: '20vh' },
    {width: 288, height: 192, minWidth: '90vw', minHeight: '16vh' }
  ];
  public maxHeight!: number;
  constructor(
    public dialog: MatDialog,
    private paramsHandlerService: ParamsHandlerService) { }

  ngOnInit(): void {
    let activeCompany = this.paramsHandlerService.getParam('company');
    if(activeCompany && activeCompany == this.company.folder) {
      this.openDialog();
    }
    if(!this.width || !this.height){
      switch(this.cardSize){
        case "small":
          this.width = this.sizes[0].width;
          this.height = this.sizes[0].height;
          this.minWidth = this.sizes[0].minWidth;
          this.minHeight = this.sizes[0].minHeight;
          break;
        case "medium":
          this.width = this.sizes[1].width;
          this.height = this.sizes[1].height;
          this.minWidth = this.sizes[1].minWidth;
          this.minHeight = this.sizes[1].minHeight;
          break;
        case "large":
          this.width = this.sizes[2].width;
          this.height = this.sizes[2].height;
          this.minWidth = this.sizes[2].minWidth;
          this.minHeight = this.sizes[2].minHeight;
          break;
      }
    }

    if (this.showStaff)
      this.maxHeight = this.height - 100;
    else
      this.maxHeight = this.height;

    if(this.isSponsor)
      this.logoHeight = 95/100 * this.height;
    else
      this.logoHeight = 80/100 * this.height;
  }

  openDialog() {
    const dialogRef = this.dialog.open(CompanyDetailsComponent, {
      data: this.company,
      height: '95vh',
      maxWidth: '95vw',
      autoFocus: false,
    });
    this.paramsHandlerService.setQueryParam('company', this.company.folder)
    dialogRef.afterClosed().subscribe(result => {
      this.paramsHandlerService.removeQueryParam('company');
    });
  }



}
