import { Component, ViewEncapsulation } from '@angular/core';
import { CompanyDetailsComponent } from '../companies/company-details/company-details.component';
import { MatDialog } from '@angular/material/dialog';
import { ParamsHandlerService } from 'src/app/global/services/params-handler/params-handler.service';
import { CompanyModel } from 'src/app/global/models/company/company.model';
import { SocialMediaModel } from 'src/app/global/models/social-media/social-media.model';
import { StaffModel } from 'src/app/global/models/company/staff.model';
import { Router } from '@angular/router';
import { AlumniPanelDialogComponent } from './alumni-panel-dialog/alumni-panel-dialog.component';
import { AlumniModel } from 'src/app/global/models/alumni/alumni.model';
import { AlumniService } from 'src/app/global/services/alumni/alumni.service';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent {
  public workshop!: CompanyModel;
  public schedulePath = 'assets/schedule/Career_Fair_2023-Full_Schedule.pdf'; // <-- change this path whenever you need to update the schedule
  public alumni: AlumniModel[] = [];
  eventName: string = 'Career Fair 2023';
  eventLocation: string = 'Computer Sciense Department, University of Crete, Heraklion, Crete, Greece';
  eventDescription: string =
  `<b>Career Fair 2023 - Meet the companies!</b> <br>
Registrations start at 11:00am. <br>
Useful Links:
    <a href="https://careerfaircsd2023.com/" target="_blank">Website</a>
    <a href="https://careerfaircsd2023.com/${this.schedulePath}" target="_blank">Schedule</a>`;
  eventStartDate: string = '20231013T120000'; // Format: 2023-08-10T14:30:00
  eventEndDate: string = '20231013T170000';   // Format: 2023-08-10T16:00:00

  get googleCalendarLink(): string {
    const startDateFormatted = this.eventStartDate;
    const endDateFormatted = this.eventEndDate;
    return `https://www.google.com/calendar/render?action=TEMPLATE&text=${encodeURIComponent(this.eventName)}&dates=${encodeURIComponent(startDateFormatted)}/${encodeURIComponent(endDateFormatted)}&details=${encodeURIComponent(this.eventDescription)}&location=${encodeURIComponent(this.eventLocation)}`;
  }

  constructor(
    public dialog: MatDialog,
    private paramsHandlerService: ParamsHandlerService,
    private router: Router,
    private alumniService: AlumniService,
    ) {
      this.createWorkshop();
      this.getAlumni();
  }

  ngOnInit(): void {
    let isWorkshopActive = this.paramsHandlerService.getParam('workshop');
    if (isWorkshopActive) {
      this.openWorkshopDialog();
    }
    let isAlumniActive = this.paramsHandlerService.getParam('alumni');
    if(isAlumniActive){
      this.openAlumniDialog();
    }
  }

  getAlumni(){
    this.alumni = this.alumniService.getAlumni();
  }

  openWorkshopDialog() {
    const dialogRef = this.dialog.open(CompanyDetailsComponent, {
      data: this.workshop,
      width: '100vw',
      height: '95vh',
      maxWidth: '95vw',
      autoFocus: false,
    });
    this.paramsHandlerService.setQueryParam('workshop', this.workshop.folder)
    dialogRef.afterClosed().subscribe(result => {
      this.paramsHandlerService.removeQueryParam('workshop');
    });
  }

  openAlumniDialog() {
    const dialogRef = this.dialog.open(AlumniPanelDialogComponent, {
      width: '100vw',
      height: '95vh',
      maxWidth: '1400px',
      autoFocus: false,
    });
    this.paramsHandlerService.setQueryParam('alumni', 'true')
    dialogRef.afterClosed().subscribe(result => {
      this.paramsHandlerService.removeQueryParam('alumni');
    });
  }

  private createWorkshop() {
    const speaker1 = new StaffModel(
      'Alexandra',
      'Karapidaki',
      'Speaker',
      'Co-Founder',
      '',
      [new SocialMediaModel('LinkedIn', 'https://www.linkedin.com/in/alexandrakarapidaki/')],
      'assets/companies/Bizrupt/Alexandra_Karapidaki.webp'
    );

    const speaker2 = new StaffModel(
      'Konstantinos',
      'Vassakis',
      'Speaker',
      'Co-Founder',
      '',
      [new SocialMediaModel('LinkedIn', 'https://www.linkedin.com/in/kvassakis/')],
      'assets/companies/Bizrupt/Konstantinos_Vassakis.webp'
    );

    const name = 'Workshop: From coding to business! By Bizrupt';
    const company = 'Bizrupt';
    const description =
    `Unlock your entrepreneurial potential in the world of computer science! Join our workshop, "From Coding to Business," at the 4th Career Fair by the Graduate Student's Association of the Computer Science Department University of Crete. <br> <br>

      Gain valuable insights into the mindset shift required to transition from being a coding expert to a successful entrepreneur or co-founder. <br>
      Discover the essential skills beyond coding that will empower you to validate business ideas, communicate effectively, solve problems, and lead with innovation. In this workshop, we'll dive into the basics you need as an entrepreneur and the viability of a business idea. <br>
      Take advantage of this opportunity to broaden your skill set and embark on an exciting journey from coding to business success! <br> <br>

      Answer to questions like: <br>
      - How to generate startup ideas and validate them <br>
      - How to build and scale successful startups <br>
      - The importance of a mindset shift from technical expert to entrepreneur <br>
      - The importance of embracing failure and learning from mistakes
      `;
    const logo = 'assets/companies/Bizrupt/logo.webp';
    const sponshorshipLevel = 'workshop';
    const website = 'https://bizrupt.gr/';
    this.workshop = new CompanyModel(
      name, // name
      '', // shortDescription
      description, // longDescription
      '', // quote
      logo, //
      sponshorshipLevel, // sponshorshipLevel
      company,
      website, //website
      [
        new SocialMediaModel('Website', website),
        new SocialMediaModel('LinkedIn', 'https://www.linkedin.com/company/27021435'),
        new SocialMediaModel('Facebook', 'https://www.facebook.com/bizrupt.gr'),
      ], //socialMedia
      null as any, //style
      [speaker1, speaker2],
      "");
  }


}
