import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlumniPanelDialogComponent } from './alumni-panel-dialog.component';

describe('AlumniPanelDialogComponent', () => {
  let component: AlumniPanelDialogComponent;
  let fixture: ComponentFixture<AlumniPanelDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AlumniPanelDialogComponent]
    });
    fixture = TestBed.createComponent(AlumniPanelDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
