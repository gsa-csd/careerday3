import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AlumniModel } from 'src/app/global/models/alumni/alumni.model';
import { AlumniService } from 'src/app/global/services/alumni/alumni.service';

@Component({
  selector: 'app-alumni-panel-dialog',
  templateUrl: './alumni-panel-dialog.component.html',
  styleUrls: ['./alumni-panel-dialog.component.scss']
})
export class AlumniPanelDialogComponent {
  public alumni: AlumniModel[] = [];
  constructor(
    public dialogRef: MatDialogRef<AlumniPanelDialogComponent>,
    private alumniService: AlumniService
  ) { }

  ngOnInit(){
    this.getAlumni();
  }

  getAlumni(){
    this.alumni = this.alumniService.getAlumni();
  }

  onClose(): void {
    this.dialogRef.close();
  }


}
