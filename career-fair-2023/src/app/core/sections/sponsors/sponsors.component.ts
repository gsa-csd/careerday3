import { Component } from '@angular/core';
import { CompanyModel } from 'src/app/global/models/company/company.model';
import { CompaniesService } from 'src/app/global/services/companies/companies.service';

@Component({
  selector: 'app-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.scss']
})
export class SponsorsComponent {
  private companies: CompanyModel[] = [];
  public sponsorsLevelA: CompanyModel[] = [];
  public sponsorsLevelB: CompanyModel[] = [];
  public sponsorsPublic: boolean = false; // change to true when sponsors are public

  constructor(
    private companiesService: CompaniesService,
  ) {
  }

  ngOnInit(): void {
    this.sponsorsLevelA = this.companiesService.getSponsorsByLevel("A");
    this.sponsorsLevelB = this.companiesService.getSponsorsByLevel("B");
  }

}
