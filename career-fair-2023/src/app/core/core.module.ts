import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponent } from './core.component';
import { CoreRoutingModule } from './core.routing';
import { MatDialogModule } from '@angular/material/dialog';
import { HomeBannerComponent } from './sections/home-banner/home-banner.component';
import { DescriptionComponent } from './sections/description/description.component';
import { CompaniesComponent } from './sections/companies/companies.component';
import { SponsorsComponent } from './sections/sponsors/sponsors.component';
import { FaqComponent } from './sections/faq/faq.component';
import { ContactComponent } from './sections/contact/contact.component';
import { OrganizingCommitteeComponent } from './sections/contact/organizing-committee/organizing-committee.component';
import { OrganizerComponent } from './sections/contact/organizing-committee/organizer/organizer.component';
import { OrganizersService } from '../global/services/organizers/organizers.service';
import { FaqCardComponent } from './sections/faq/faq-card/faq-card.component';
import { FaqsService } from '../global/services/faqs/faqs.service';
import { CompanyCardComponent } from './sections/companies/company-card/company-card.component';
import { StaffBubbleComponent } from './sections/companies/staff-bubble/staff-bubble.component';
import { CompanyDetailsComponent } from './sections/companies/company-details/company-details.component';
import { StaffCardComponent } from './sections/companies/staff-card/staff-card.component';
import { AlumniPanelDialogComponent } from './sections/description/alumni-panel-dialog/alumni-panel-dialog.component';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  declarations: [
    CoreComponent,
    HomeBannerComponent,
    DescriptionComponent,
    CompaniesComponent,
    SponsorsComponent,
    FaqComponent,
    ContactComponent,
    OrganizingCommitteeComponent,
    OrganizerComponent,
    FaqCardComponent,
    CompanyCardComponent,
    StaffBubbleComponent,
    CompanyDetailsComponent,
    StaffCardComponent,
    AlumniPanelDialogComponent,
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    MatDialogModule,
    SharedModule,
  ],
  providers: [
    OrganizersService,
    FaqsService
  ]
})
export class CoreModule { }
