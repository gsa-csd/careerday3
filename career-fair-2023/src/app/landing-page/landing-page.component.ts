import { Component } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent {
  public schedulePath = 'assets/schedule/Career_Fair_2023-Full_Schedule.pdf'; // <-- change this path whenever you need to update the schedule
  public feedbackFormPath = 'https://forms.gle/FZQrptcxzUigafqp6';
}
