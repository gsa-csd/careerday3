import { Injectable } from '@angular/core';
import { OrganizerModel } from '../../models/organizer/organizer.model';
import * as organizersData from '../../../../assets/data/organizing-committee.json';

@Injectable({
  providedIn: 'root'
})
export class OrganizersService {
  init_data: any = organizersData;
  data: OrganizerModel[] = [];

  constructor() {
    this.loadOrganizersFromFile();
  }

  loadOrganizersFromFile() {
    for (let i = 0; i < this.init_data.length; i++) {
      var { name, surname, role, affiliation, socialMedia, imageURL } = this.init_data[i];
      let imagePath = imageURL ? 'assets/committee/' + imageURL : '';
      this.data.push(new OrganizerModel(name, surname, role, affiliation, socialMedia, imagePath));
    }
  }

  getOrganizers(): any {
    return this.data;
  }
}
