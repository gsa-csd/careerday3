import { Injectable } from '@angular/core';
import * as volunteersData from '../../../../assets/data/volunteers.json';
import { OrganizerModel } from '../../models/organizer/organizer.model';
@Injectable({
  providedIn: 'root'
})
export class VolunteersService {
  init_data: any = volunteersData;
  data: OrganizerModel[] = [];
  constructor() {
    this.loadVolunteersFromFile();
  }

  loadVolunteersFromFile() {
    for (let i = 0; i < this.init_data.length; i++) {
      var { name, surname, affiliation, socialMedia, imageURL} = this.init_data[i];
      let imagePath = imageURL ? "assets/volunteers/" + imageURL : '';
      this.data.push(new OrganizerModel(name, surname, '', affiliation, socialMedia, imagePath));
    }
  }

  getVolunteers(): any {
    return this.data;
  }

}
