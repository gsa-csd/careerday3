import { Injectable } from '@angular/core';
import { AlumniModel } from '../../models/alumni/alumni.model';
import * as alumniData from '../../../../assets/data/alumni.json';
@Injectable({
  providedIn: 'root'
})
export class AlumniService {
  init_data: any = alumniData;
  data: AlumniModel[] = [];
  constructor() {
    this.loadAlumniFromFile();
  }

  loadAlumniFromFile() {
    for (let i = 0; i < this.init_data.length; i++) {
      var { name, surname, email, position, bio, socialMedia, image, company } = this.init_data[i];
      this.data.push(new AlumniModel(name, surname, email, position, bio, socialMedia, image, company));
    }
  }

  getAlumni(): any {
    return this.data;
  }
}
