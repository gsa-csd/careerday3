import { Injectable } from '@angular/core';
import * as companiesData from '../../../../assets/companies/metadata.json';
import { CompanyModel } from '../../models/company/company.model';
import { StaffModel } from '../../models/company/staff.model';
@Injectable({
  providedIn: 'root'
})
export class CompaniesService {

  init_data: any = companiesData;
  data: CompanyModel[] = [];

  constructor() {
    this.loadCompaniesFromFile();
    // remove to add OTE
    // this.data = this.data.filter(company => company.name != "OTE");

  }

  loadCompaniesFromFile(){
    for (let i = 0; i < this.init_data.length; i++) {
      // var {name, shortDescription, longDescription, quote, logo, sponsorshipLevel, website, socialMedia, style, staff} = this.init_data[i];
      // this.data.push(new CompanyModel(name, shortDescription, longDescription, quote, logo, sponsorshipLevel, website, socialMedia, style, staff));
      let companyName = this.init_data[i].company.name;
      let longDescription = this.init_data[i].company.description;
      let shortDescription = this.init_data[i].company.teaser;
      let quote = this.init_data[i].company.quote;
      let website = this.init_data[i].company.website;
      let folder = this.init_data[i].company.folder;
      let logo = `assets/companies/${folder}/logo.webp`;
      let sponsorshipLevel = this.init_data[i].company.sponsorship_level;
      let companySocialMedia = [ // or whatever else we have
        { name: "Website", link: this.init_data[i].company.website},
        { name: "LinkedIn", link: this.init_data[i].company.linkedin },
        { name: "Facebook", link: this.init_data[i].company.facebook }
      ]
      let staff: StaffModel[] = [];
      for (let j = 0; j < this.init_data[i].staff.length; j++) {
        let staffName = this.init_data[i].staff[j].name;
        let staffSurname = this.init_data[i].staff[j].surname;
        let position = this.init_data[i].staff[j].position;
        let email = this.init_data[i].staff[j].email;
        let staffSocialMedia = [ // given that we have linkedin and email for staff
          { name: "LinkedIn", link: this.init_data[i].staff[j].linkedin },
          { name: "Email", link: this.init_data[i].staff[j].email},
        ]
        let imageURL = `assets/companies/${folder}/${staffName}_${staffSurname}.webp`;
        let role = this.init_data[i].staff[j].speaker ? "Speaker" : "Booth Staff";
        staff.push(new StaffModel(staffName, staffSurname, role, position, email, staffSocialMedia, imageURL));
      }
      let style = this.init_data[i].company.style;

      let pdfURL = this.init_data[i].company.pdfURL ? `assets/companies/${folder}/${this.init_data[i].company.pdfURL}` : '';
      // check if pdfURL is an https link, if so, leave the link as is
      // this is a crappy last minute solution that seems to work temporarily
      // Should not be used in the future!
      // ALSO NOTE: THIS WON'T WORK FOR HTTP LINKS AND IT SHOULDN'T. IF THEY HAVE HTTP, WE DON'T TRUST THEM :)
      if(this.init_data[i].company.pdfURL && this.init_data[i].company.pdfURL.startsWith("https://")){
        pdfURL = `${this.init_data[i].company.pdfURL}`;
      }
      this.data.push(new CompanyModel(companyName, shortDescription, longDescription, quote, logo, sponsorshipLevel, folder, website, companySocialMedia, style, staff, pdfURL));
    }
  }

  getCompanies(sorted: boolean = true, sponsorsFirst: boolean = true, workshopFirst: boolean = true): CompanyModel[]{
    let companies:CompanyModel[] = this.data;
    let result:CompanyModel[] = [];
    if(sorted)
      companies = this.data.sort((a, b) => a.name.localeCompare(b.name));

    if(sponsorsFirst){
      let sponsorsLevelA = companies.filter(company => company.sponsorshipLevel == "A");
      let sponsoreLevelB = companies.filter(company => company.sponsorshipLevel == "B");
      let standard = companies.filter(company => company.sponsorshipLevel == "Standard package");
      result = [...sponsorsLevelA, ...sponsoreLevelB, ...standard];
      // companies will first contain sponsoreLevelA, then the rest of the companies in alphabetical order (if sorted == true)
      if(workshopFirst){
        let workshop = companies.filter(company => company.sponsorshipLevel == "Workshop");
        // companies will first contain workshops, then the rest of the companies in alphabetical order (if sorted == true)
        result = workshop.concat(result);
      }
    }

    return result;
  }

  // get sponsors by level, ordered by name
  getSponsorsByLevel(level: string): CompanyModel[]{
    return this.data.filter(company => company.sponsorshipLevel == level).sort((a, b) => a.name.localeCompare(b.name));
  }



}

