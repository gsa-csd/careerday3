import { Injectable } from '@angular/core';
import { FaqModel } from '../../models/faq/faq.model';
import * as faqData from '../../../../assets/data/faq.json';

@Injectable({
  providedIn: 'root'
})
export class FaqsService {

  init_data: any = faqData;
  data: FaqModel[] = [];

  constructor() {
    this.loadFaqFromFile();
  }

  loadFaqFromFile(){
    for (let i = 0; i < this.init_data.length; i++) {
      var {question, answer} = this.init_data[i];
      this.data.push(new FaqModel(question, answer));
    }
  }

  getFaqs(): FaqModel[]{
    return this.data;
  }

}
