import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ParamsHandlerService {

  constructor(
    private route: ActivatedRoute,
    private router: Router) { }

  public getParam(param: string): string {
    return this.route.snapshot.queryParams[param] || '';
  }

  public getQueryParams(): any {
    return this.route.snapshot.queryParams;
  }

  public setQueryParam(param: string, value: string): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        [param]: value
      },
      queryParamsHandling: 'merge'
    });
  }

  public removeQueryParam(param: string): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        [param]: null
      },
      queryParamsHandling: 'merge'
    });
  }
}
