import { SocialMediaModel } from "../social-media/social-media.model";

export class AlumniModel{
  public name: string;
  public surname: string;
  public email: string;
  public position: string;
  public bio: string;
  public socialMedia: SocialMediaModel[] = [];
  public image: string;
  public company: string;

  constructor(name: string, surname: string, email: string, position: string, bio: string, socialMedia: SocialMediaModel[], image: string, company: string) {
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.position = position;
    this.bio = bio;
    this.socialMedia = socialMedia;
    this.image = image;
    this.company = company;
  }
}
