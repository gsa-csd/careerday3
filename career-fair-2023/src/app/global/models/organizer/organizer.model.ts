export class OrganizerModel {
  name: string;
  surname: string;
  role: string;
  affiliation: string;
  socialMedia: { name: string, link: string }[];
  imageURL: string;

  constructor(name: string, surname: string, role: string, affiliation: string, socialMedia: { name: string, link: string }[], imageURL: string) {
    this.name = name;
    this.surname = surname;
    this.role = role;
    this.affiliation = affiliation;
    this.socialMedia = socialMedia;
    this.imageURL = imageURL;

  }
}

