export class TestimonialModel {
  author: string;
  role: string;
  text: string;

  constructor(author: string, role: string, text: string) {
    this.author = author;
    this.role = role;
    this.text = text;
  }

}
