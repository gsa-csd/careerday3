export class SocialMediaModel {
    name: string;
    link: string;
    constructor(name:string, link:string) {
        this.name = name;
        this.link = link;
    }
}
