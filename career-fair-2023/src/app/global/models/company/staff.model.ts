export class StaffModel {
  name: string;
  surname: string;
  role: string; // role in the career Fair Event
  position: string; // position in the company
  email: string;
  socialMedia: { name: string, link: string }[];
  imageURL: string;

  constructor(
    name: string,
    surname: string,
    role: string,
    position: string,
    email: string,
    socialMedia: {
      name: string,
      link: string
    }[],
    imageURL: string
  ) {
    this.name = name;
    this.surname = surname;
    this.role = role;
    this.position = position;
    this.email = email;
    this.socialMedia = socialMedia;
    this.imageURL = imageURL;
  }

}
