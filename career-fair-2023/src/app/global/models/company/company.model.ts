import { SocialMediaModel } from '../social-media/social-media.model';
import { StaffModel } from './staff.model';

class CompanyCardStyle {
  bgColor: string;
  top: number;
  left: number;
  logoWidth: number;
  logoHeight: number;
  aspectRatio: string;
  constructor(bgColor: string, top: number, left: number, width: number, height: number, aspectRatio: string) {
    this.bgColor = bgColor;
    this.top = top;
    this.left = left;
    this.logoWidth = width;
    this.logoHeight = height;
    this.aspectRatio = aspectRatio;
  }
}

export class CompanyModel {
  name: string;
  shortDescription: string;
  longDescription: string;
  quote: string;
  logo: string;
  sponsorshipLevel: string;
  folder: string;
  website: string;
  socialMedia: SocialMediaModel[];
  style: CompanyCardStyle;
  staff: StaffModel[];
  pdfURL: string;
  constructor(
    name: string,
    shortDescription: string,
    longDescription: string,
    quote: string,
    logo: string,
    sponsorshipLevel: string,
    folder: string,
    website: string,
    socialMedia: SocialMediaModel[],
    style: CompanyCardStyle,
    staff: StaffModel[],
    pdfURL: string
  ) {
    this.name = name;
    this.shortDescription = shortDescription;
    this.longDescription = longDescription;
    this.quote = quote;
    this.logo = logo;
    this.sponsorshipLevel = sponsorshipLevel;
    this.folder = folder;
    this.website = website;
    this.socialMedia = socialMedia;
    this.style = style;
    this.staff = staff;
    this.pdfURL = pdfURL;
  }
}
