import { Component } from '@angular/core';
import { TestimonialModel } from 'src/app/global/models/testimonial/testimonial.model';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-testimonials-carousel',
  templateUrl: './testimonials-carousel.component.html',
  styleUrls: ['./testimonials-carousel.component.scss']
})
export class TestimonialsCarouselComponent {
  public testimonials: TestimonialModel[] = [];
  public testimonialsPublic: boolean = false;

  constructor() { }
  ngOnInit() {
    this.generateTestimonials();
    this.init();
  }

  init() {
    let carousels = () => {
      $(".owl-carousel1").owlCarousel({
        loop: true,
        center: true,
        margin: 0,
        responsiveClass: true,
        nav: false,
        autoplay: true,
        autoplayTimeout: 8000,
        responsive: {
          0: {
            items: 1,
            nav: true
          },
          680: {
            items: 2,
            nav: true,
            loop: true
          },
          1000: {
            items: 3,
            nav: true
          }
        }
      });
    };
    carousels();
  };

  generateTestimonials() {
    this.testimonials.push(
      new TestimonialModel(
        "Prof. Evangelos Markatos",
        "CSD, UoC",
        "The CSD Career Fair gives the opportunity to a wide range of employers to tap into the top talent pool of the students of the Computer Science Department of the University of Crete, to present their current and future job openings, and to recruit future employees."
      ));
    this.testimonials.push(
      new TestimonialModel(
        "Prof. Angelos Bilas",
        "CSD, UoC",
        "The CSD Career Fair has very quickly become an event that creates real opportunity by connecting new talent to industry and addressing needs that matter. We look forward to the 2023 event this coming October!"
      ));
    this.testimonials.push(
      new TestimonialModel(
        "Prof. Antonis Argyros",
        "Chair of CSD, UoC",
        "The CSD Career Fair is an excellent initiative of the graduate students of our Department that has now become an institution. In this meeting, the students of the Department have the opportunity to get to know many important companies that employ computer scientists and to interact with their executives. The experience gained from the previous years shows that the CSD Career Fair is a very useful and successful event. I am confident that this year's event will also be a huge success!"
      ));
    this.testimonials.push(
      new TestimonialModel(
        "Nikos Batsaras",
        "Software Engineer",
        "A great experience overall ! Had the chance to talk to leading professionals of different fields and get a better understanding of the industry and the opportunities it offers. Would definitely recommend to newcomers in the industry !"
      ));
  }
}
