import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-social-media-icon',
  templateUrl: './social-media-icon.component.html',
  styleUrls: ['./social-media-icon.component.scss']
})
export class SocialMediaIconComponent {
  @Input() socialMedia!: { name: string, link: string };
  @Input() fontSize!: string;
  @Input() linkVisible: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  public getIcon(): string {
    switch (this.socialMedia.name) {
      case 'LinkedIn':
        return 'bi bi-linkedin';
      case 'Facebook':
        return 'bi bi-facebook';
      case 'Instagram':
        return 'bi bi-instagram';
      case 'Twitter':
        return 'bi bi-twitter';
      case 'GitHub':
        return 'bi bi-github';
      case 'Website':
        return 'bi bi-globe';
      case 'Email':
        return 'bi bi-envelope-fill';
      default:
        return 'bi bi-question';
    }
  }
}
