import { Component, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
  public isLandingPage = false;
  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.isLandingPage = this.route.snapshot.url[0]?.path === 'landing-page';
  }

  @HostListener('window:scroll', ['$event'])
  onScrollEvent($event: any) {
    // Get all sections on the page
    const sections = document.querySelectorAll('div[id]');
    let currentSection = '';

    // Loop through sections to find current section
    sections.forEach((section: any) => {
      const sectionTop = section.offsetTop - 80;
      const sectionHeight = section.offsetHeight;
      if (window.pageYOffset >= sectionTop && window.pageYOffset < sectionTop + sectionHeight) {
        currentSection = section.getAttribute('id');
      }
    });

    // Get all navbar links
    const navLinks = document.querySelectorAll('.nav-link');

    // Remove active class from all links
    navLinks.forEach(link => {
      link.classList.remove('active');
    });

    // Add active class to corresponding link
    let correspondingLink = document.querySelector(`.nav-link[href="#${currentSection}"]`);
    const scrollHeight = document.documentElement.scrollHeight;
    const scrollTop = document.documentElement.scrollTop;
    const clientHeight = document.documentElement.clientHeight;

    if (scrollTop + clientHeight === scrollHeight) {
      correspondingLink = document.querySelector('.nav-link[href="#footer"]');
    }

    if (correspondingLink) {
      correspondingLink.classList.add('active');
    }


  }

  onLinkClick(event: any) {
    // Get all navbar links
    const navLinks = document.querySelectorAll('.nav-link');

    // Remove active class from all links
    navLinks.forEach(link => {
      link.classList.remove('active');
    });

    // Add active class to clicked link
    event.target.classList.add('active');
  }
}
