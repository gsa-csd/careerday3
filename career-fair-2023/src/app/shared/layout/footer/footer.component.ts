import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  constructor(private router: Router){}

  scrollToTop() {
    // Get the current scroll position
    const currentScroll = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;

    // Define the target scroll position (top of the page)
    const targetScroll = 0;

    // Define the duration of the smooth scroll animation (in milliseconds)
    const duration = 500; // 0.5 seconds

    // Calculate the total distance to scroll
    const distance = targetScroll - currentScroll;

    // Get the current timestamp
    const startTimestamp = performance.now();

    // Define the easing function
    const easeInOutQuad = (t: number) => t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;

    // Create an animation function to scroll smoothly
    const animation = (timestamp: number) => {
      // Calculate the elapsed time since the start of the animation
      const elapsed = timestamp - startTimestamp;

      // Calculate the progress of the animation based on the elapsed time
      const progress = easeInOutQuad(Math.min(1, elapsed / duration));

      // Update the scroll position based on the progress of the animation
      window.scrollTo(0, currentScroll + distance * progress);

      // If the animation is not complete, continue the animation
      if (elapsed < duration) {
        requestAnimationFrame(animation);
      }
    };

    // Start the animation
    requestAnimationFrame(animation);
  }

}
