import { Component } from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent {
  // implement logic to implement a countdown timer until october 13th 2023 10:00am

  public date: Date = new Date('October 13, 2023 11:00:00');
  public remainingTime: any = {};
  public eventHasStarted: boolean = false;
  public eventHasEnded: boolean = false;
  constructor() { }

  ngOnInit(): void {
    this.eventHasEnded = true;
    this.remainingTime = { days: 0, hours: 0, minutes: 0, seconds: 0 };
    // remove comments to start the timer countdown
    // setInterval(() => {
    //   this.remainingTime = this.calculateRemainingTime();
    // }, 1000);
  }

  public calculateRemainingTime(): any {
    const now = Date.now();
    const timeDifference = this.date.getTime() - now;
    if(timeDifference < 0) {
      this.eventHasStarted = true;
      return { days: 0, hours: 0, minutes: 0, seconds: 0 };
    }
    const days = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
    const hours = Math.floor((timeDifference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((timeDifference % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((timeDifference % (1000 * 60)) / 1000);

    return { days, hours, minutes, seconds };
  }
}
