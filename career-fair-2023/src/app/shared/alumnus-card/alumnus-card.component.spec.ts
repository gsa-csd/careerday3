import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlumnusCardComponent } from './alumnus-card.component';

describe('AlumnusCardComponent', () => {
  let component: AlumnusCardComponent;
  let fixture: ComponentFixture<AlumnusCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AlumnusCardComponent]
    });
    fixture = TestBed.createComponent(AlumnusCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
