import { Component, Input } from '@angular/core';
import { AlumniModel } from 'src/app/global/models/alumni/alumni.model';

@Component({
  selector: 'app-alumnus-card',
  templateUrl: './alumnus-card.component.html',
  styleUrls: ['./alumnus-card.component.scss']
})
export class AlumnusCardComponent {
  @Input() alumnus!: AlumniModel;
  @Input() isCard: boolean = true;
  constructor() { }

  ngOnInit(): void {
  }
}
