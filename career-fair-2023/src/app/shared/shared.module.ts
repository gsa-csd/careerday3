import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlumnusCardComponent } from './alumnus-card/alumnus-card.component';
import { CountdownComponent } from './countdown/countdown.component';
import { SocialMediaIconComponent } from './social-media-icon/social-media-icon.component';
import { SponsorMedalComponent } from './sponsor-medal/sponsor-medal.component';
import { TestimonialsCarouselComponent } from './testimonials-carousel/testimonials-carousel.component';
import { MenuComponent } from './layout/menu/menu.component';
import { FooterComponent } from './layout/footer/footer.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    AlumnusCardComponent,
    CountdownComponent,
    SocialMediaIconComponent,
    SponsorMedalComponent,
    TestimonialsCarouselComponent,
    MenuComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    AlumnusCardComponent,
    CountdownComponent,
    SocialMediaIconComponent,
    SponsorMedalComponent,
    TestimonialsCarouselComponent,
    MenuComponent,
    FooterComponent,
  ]
})
export class SharedModule { }
