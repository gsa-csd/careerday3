import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SponsorMedalComponent } from './sponsor-medal.component';

describe('SponsorMedalComponent', () => {
  let component: SponsorMedalComponent;
  let fixture: ComponentFixture<SponsorMedalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SponsorMedalComponent]
    });
    fixture = TestBed.createComponent(SponsorMedalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
