import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-sponsor-medal',
  templateUrl: './sponsor-medal.component.html',
  styleUrls: ['./sponsor-medal.component.scss']
})
export class SponsorMedalComponent {
  @Input() sponsorshipLevel:string = "";
  @Input() scale: number = 1;


}
