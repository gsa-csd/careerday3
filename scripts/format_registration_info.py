
import os.path
import io 
import json
import pandas as pd 

from PIL import Image
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload

BASE_DIR='./companies'
DATA_FILE='data/company_registration_info.csv'

def main():
    ''' This is the API for google drive '''
    service = get_service()
    
    ''' Create base directory to store companies and metadata'''
    if not os.path.exists(BASE_DIR):
        os.mkdir(BASE_DIR)

    ''' Read registrations from google sheets '''
    df = pd.read_csv(DATA_FILE)
    ''' Drop unnecessary columns'''
    df = df.drop(['Add members','Add a second member','Add a third member','Add a fourth member','Add a fifth member'], axis=1)
    
    print('\nStarting companies info extraction')
    print('==================================\n')
    ''' This var holds all the metadata for companies that will be exported in a .json file '''
    metadata = []
    for index, row in df.iterrows():
        company = extract_company_info(row)
        staff = extract_staff_info(row)

        company_name = company['Company Name']
        print(f'Extracting: {company_name}')
        company_folder = company_name.replace(' ', '_') 
        company_folder = company_folder.replace('/', '_')
        company_folder = company_folder.replace('(', '_')
        company_folder = company_folder.replace(')', '_')
        
        company_base_path = f'{BASE_DIR}/{company_folder}'
        if os.path.exists(company_base_path):
            print(f'\t Already exists')
            continue
        
        print('\tCreating folder')
        os.mkdir(company_base_path)

        print('\tDownloading logo')
        id = link_to_id(company['Logo'])
        download_image(service, id, f'{company_base_path}/logo.webp') # change to png/jpg if needed

        staff_info = []
        for member in staff:
            name = member[0]
            surname = member[1]
            if type(name) == str: # only way found for comparison :P  
                name = name.replace(' ','')
                surname = surname.replace(' ','')
                
                print(f'\tExtracting info for: {name} {surname}')
                id = link_to_id(member[5])
                download_image(service, id, f'{company_base_path}/{name}_{surname}.webp') # change to png/jpg if needed

                staff_info.append({
                    'name': name,
                    'surname': surname,
                    'email': member[2],
                    'position': member[3],
                    'linkedin': member[4],
                    'speaker': ('Photo speaker' in member)
                })
        description = return_if_exists(company[1])
        description = description.replace('\n', '<br>')
        quote = return_if_exists(company[8])
        quote = quote.replace('\n', '<br>')
        pdfURL = '' # read it from excel next year if it is integrated in the same form
        metadata.append({
            'company': {
                'name': company_name,
                'description': description,
                'folder': company_folder,
                'website': return_if_exists(company[2]),
                'linkedin': return_if_exists(company[4]),
                'facebook': return_if_exists(company[5]),
                'instagram': return_if_exists(company[6]),
                'teaser': return_if_exists(company[7]),
                'quote': quote,
                'sponsorship_level': return_if_exists(company[9]),
                'style': {
                     'bgColor': '#ffffff'
                },
                'pdfURL': pdfURL
            },
            'staff': staff_info
        })
        print('\tDone\n')
    
    print('==================================')
    print('Extraction finished')
    print('Writing metadata file...')
    with open(f'{BASE_DIR}/metadata.json', 'w') as f:
        json.dump(metadata, f, indent=4)

        
def get_service():
    # Careful with this scope, because it give unlimited access 
    scopes = ['https://www.googleapis.com/auth/drive.readonly']

    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', scopes)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'google_cloud_cred.json', scopes)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())

    return build('drive', 'v3', credentials=creds)


def download_image(service, id: str, save_img_as: str) -> None:
    request = service.files().get_media(fileId=id)
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fd=fh, request= request)
    
    done = False
    while not done:
        status, done= downloader.next_chunk()

    fh.seek(0)

    with open(save_img_as, 'wb') as f:
        f.write(fh.read())
    # Remove comments to try image compression (experimental)
    input_image_path = save_img_as
    output_image_path = save_img_as
    max_size = (800, 600)  # Set the maximum size for the compressed image (width, height)
    compress_image(input_image_path, output_image_path, max_size)

def link_to_id(link) -> str:
            return str(link).split('=')[1]


def return_if_exists(cell) -> str:
            return cell if type(cell) == str else ''


def extract_company_info(row):
     return row[1:11]


def extract_staff_info(row, num_members: int=6):
    staff = [] 
    start = 11
    staff_num_cols = 6
    end = start+staff_num_cols
    for _ in range(num_members):
        staff.append(row[start:end])
        start = end
        end += staff_num_cols
    return staff

def compress_image(input_path, output_path, max_size):
    # Open the image
    image = Image.open(input_path)

    # Convert image to RGB color mode
    image = image.convert("RGBA")

    # Set the maximum size (width or height) for the compressed image
    max_width, max_height = max_size

    # Calculate the aspect ratio to maintain the image's original proportions
    width, height = image.size
    aspect_ratio = width / height

    # Calculate the new width and height based on the maximum size and aspect ratio
    if width > max_width or height > max_height:
        if aspect_ratio > 1:
            new_width = max_width
            new_height = int(max_width / aspect_ratio)
        else:
            new_width = int(max_height * aspect_ratio)
            new_height = max_height
        resized_image = image.resize((new_width, new_height), Image.LANCZOS)
    else:
        # No need to resize if the image is already smaller than the maximum size
        resized_image = image

    # Save the compressed image as JPEG
    resized_image.save(output_path, optimize=True, quality=85)

    # Print the compression statistics
    input_size = os.path.getsize(input_path)
    output_size = os.path.getsize(output_path)
    compression_ratio = round(output_size / input_size, 2)
    print(f"Compressed image saved to {output_path}")
    print(f"Compression ratio: {compression_ratio}")

if __name__ == '__main__':
    main() 